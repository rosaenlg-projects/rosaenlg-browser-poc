const { src, dest, parallel, series } = require('gulp');
const { exec } = require('child_process');

const fs = require('fs');
const gulpRosaenlgHelpers = require('gulp-rosaenlg');

const rosaeNlgVersion = "1.8.0";

function init(cb) {
  const folders = [
    'dist',
  ];
  folders.forEach(dir => {
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
      console.log('📁  folder created:', dir);
    }
  });
  cb();
}

function clean(cb) {
  exec('rm -rf dist');
  cb();
}


function copyStaticElts() {
  return src([
    `node_modules/rosaenlg/dist/rollup/rosaenlg_tiny_fr_FR_${rosaeNlgVersion}.js`,
    `node_modules/rosaenlg/dist/rollup/rosaenlg_tiny_en_US_${rosaeNlgVersion}.js`,
    `node_modules/rosaenlg/dist/rollup/rosaenlg_tiny_de_DE_${rosaeNlgVersion}.js`,
    `node_modules/rosaenlg/dist/rollup/rosaenlg_tiny_it_IT_${rosaeNlgVersion}.js`,
    `node_modules/rosaenlg/dist/rollup/rosaenlg_tiny_OTHER_${rosaeNlgVersion}.js`,
    'src/browser_fr_FR.html',
    'src/browser_en_US.html',
    'src/browser_de_DE.html',
    'src/browser_OTHER.html',
    'src/browser_it_IT.html',
  ])
    .pipe(dest('dist/'));
}

function compile_templates_fr_FR(cb) {
  return gulpRosaenlgHelpers.compileTemplates(
    [{ source: 'src/fruits_fr_FR.pug', name: 'fruits_fr_FR' }],
    'fr_FR',
    'dist/compiled_fr_FR.js',
    'templates_holder',
    true
  )
}

function compile_templates_en_US(cb) {
  return gulpRosaenlgHelpers.compileTemplates(
    [{ source: 'src/fruits_en_US.pug', name: 'fruits_en_US' }],
    'en_US',
    'dist/compiled_en_US.js',
    'templates_holder',
    true
  )
}

function compile_templates_de_DE(cb) {
  return gulpRosaenlgHelpers.compileTemplates(
    [{ source: 'src/fruits_de_DE.pug', name: 'fruits_de_DE' }],
    'de_DE',
    'dist/compiled_de_DE.js',
    'templates_holder',
    true
  )
}

function compile_templates_it_IT(cb) {
  return gulpRosaenlgHelpers.compileTemplates(
    [{ source: 'src/template_it_IT.pug', name: 'template_it_IT' }],
    'it_IT',
    'dist/compiled_it_IT.js',
    'templates_holder',
    true
  )
}

function compile_templates_OTHER(cb) {
  return gulpRosaenlgHelpers.compileTemplates(
    [{ source: 'src/fruits_OTHER.pug', name: 'fruits_OTHER' }],
    'OTHER',
    'dist/compiled_OTHER.js',
    'templates_holder',
    true
  )
}

exports.all = series(init, parallel(copyStaticElts, compile_templates_it_IT, compile_templates_OTHER, compile_templates_de_DE, compile_templates_fr_FR, compile_templates_en_US));
exports.clean = clean;
