
# RosaeNLG Browser POC

Client side (in browser) NLG using RosaeNLG.

This technical POC shows how to generate texts in client side, _in a browser_, with RosaeNLG, and _without node.js_.

## Client side rendering

Compilation occurs on server side when packaging the app. Rendering is made in the browser.

### How it works

The general idea is:

* to author text templates in a defined environment (for instance node.js based)
* then to compile and pack the templates
* and to run them in browser based environment, with no connection to the authoring environment (without using node.js)

`browserify` makes most of the job. The specific point is that the PUG RosaeNLG template is first compiled before being packaged.

The technical process is the following:

* `compile_templates_*` in `gulpfile.js/index.js` compiles and packages the template thanks to `gulp-rosaenlg` module
* RosaeNLG provides a browser ready package per language, here `rosaenlg_tiny_en_US_VERSION.js` for `en_US`
* `browser_en_US.html` or `browser_fr_FR.html` simply renders the template using sample data, and shows the result


### Usage

* `npm run build` to pack everything
* open `dist/browser_en_US.html` in a browser
* you should see `<p>Apples, bananas and apricots</p>` in the text area - this text is generated on the fly in the browser

## Client side compilation and rendering

Both compilation and rendering are made in the browser.

### How it works

Templates can be authored directly in the browser, compiled in the browser and the texts can be rendered in the browser.

To be able to do that, you must use a js version of RosaeNLG that is larger, with `_comp at the end`, like `rosaenlg_tiny_fr_FR_1.0.1_comp.js`:

* it includes the templates compiler
* but also all linguistic resources that are associated with a language

TIP: Client side compilation is for exotic usecases. You should favor server side compilation.

### Usage

* `npm run build` to pack everything
* open `dist/browser_fr_FR_comp.html` in a browser
* you should see the template on the left; press `Compile and render` - the template is compiled and the text is generated on the right. Feel free to edit the template.
