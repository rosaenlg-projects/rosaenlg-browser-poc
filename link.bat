@echo off
if not "%1"=="am_admin" (powershell start -verb runas '%0' am_admin & exit /b)

cd c:\rosaenlg\rosaenlg-browser-poc
mklink /D node_modules\rosaenlg c:\rosaenlg\rosaenlg\packages\rosaenlg
mklink /D node_modules\gulp-rosaenlg c:\rosaenlg\rosaenlg\packages\gulp-rosaenlg

pause
